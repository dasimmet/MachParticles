# Particle Simultations on Mach

```
git clone --recurse-submodules
zig build run -Ddependencies
```

## Run a prebuilt version

```
chmod +x ./MachParticles
./MachParticles <number of particles>
```

## Controls

* `mouse move`: gravity center
* `mouse wheel`: scale mouse gravity
* `mouse left/right`: reverse/disable mouse gravity
* `hold shift`: decrease values
* `tab`: toggle edge wrap
* `1-7`: scale parameters in order of SimParams struct