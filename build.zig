const std = @import("std");
const mach = @import("libs/mach/build.zig");
const Dep = std.build.ModuleDependency;

const this_dir = thisDir();

inline fn thisDir() []const u8 {
    return comptime std.fs.path.dirname(@src().file) orelse ".";
}

pub fn build(b: *std.build.Builder) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});
    const options = mach.Options{};

    if (b.option(bool, "dependencies", "ensure Dependencies on build") orelse false){
        try ensureDependencies(b.allocator);
    }
    const default_num_particle = b.option(u32, "default_num_particle", "default number of particles") orelse 5000;
    const build_options = b.addOptions();
    build_options.addOption(u32, "default_num_particle", default_num_particle);

    const app_src = .{
        .name = "MachParticles",
        .deps = .{
            Dependency.clap,
            Dependency.zmath,
            Dependency.zigimg,
        },
        .std_platform_only = false,
        .has_assets = false,
        .use_freetype = false,
    };

    var deps = std.ArrayList(Dep).init(b.allocator);
    try deps.append(Dep{
        .name = "build_options",
        .module = build_options.createModule(),
    });
    inline for (app_src.deps) |d| {
        try deps.append(d.moduleDependency(b));
    }

    const app = try mach.App.init(
        b,
        .{
            .name = app_src.name,
            .src = "src/main.zig",
            .target = target,
            .optimize = optimize,
            .deps = deps.items,
            .res_dirs = if (app_src.has_assets) &.{app_src.name ++ "/assets"} else null,
            .watch_paths = &.{"src/"},
            .use_freetype = if (app_src.use_freetype) "freetype" else null,
        },
    );
    app.step.addOptions("build_options", build_options);

    try app.link(options);
    app.install();

    const compile_step = b.step(app_src.name, "Compile " ++ app_src.name);
    compile_step.dependOn(&app.getInstallStep().?.step);

    const run_cmd = try app.run();
    run_cmd.dependOn(compile_step);
    const run_step = b.step("run", "Run " ++ app_src.name);
    run_step.dependOn(run_cmd);

    const compile_all = b.step("compile", "Compile Everything");
    compile_all.dependOn(b.getInstallStep());
}

const Dependency = enum {
        zmath,
        zigimg,
        clap,

    pub fn moduleDependency(dep: @This(), b2: *std.Build) Dep {
        const path = switch (dep) {
        .zmath => "libs/zmath/src/zmath.zig",
        .zigimg => "libs/zigimg/zigimg.zig",
        .clap => "libs/clap/clap.zig",
        };
        return std.Build.ModuleDependency{
            .name = @tagName(dep),
            .module = b2.createModule(.{.source_file = .{.path = path}}),
        };
    }
};

pub fn copyFile(app_src_path: []const u8, dst_path: []const u8) void {
    std.fs.cwd().makePath(std.fs.path.dirname(dst_path).?) catch unreachable;
    std.fs.cwd().copyFile(app_src_path, std.fs.cwd(), dst_path, .{}) catch unreachable;
}

fn sdkPath(comptime suffix: []const u8) []const u8 {
    if (suffix[0] != '/') @compileError("suffix must be an absolute path");
    return comptime blk: {
        const root_dir = std.fs.path.dirname(@src().file) orelse ".";
        break :blk root_dir ++ suffix;
    };
}

fn ensureDependencies(allocator: std.mem.Allocator) !void {
    ensureGit(allocator);
    try ensureSubmodule(allocator, "libs/mach");
    try ensureSubmodule(allocator, "libs/zmath");
    try ensureSubmodule(allocator, "libs/zigimg");
}

fn ensureSubmodule(allocator: std.mem.Allocator, path: []const u8) !void {
    if (std.process.getEnvVarOwned(allocator, "NO_ENSURE_SUBMODULES")) |no_ensure_submodules| {
        defer allocator.free(no_ensure_submodules);
        if (std.mem.eql(u8, no_ensure_submodules, "true")) return;
    } else |_| {}
    var child = std.ChildProcess.init(&.{ "git", "submodule", "update", "--init", path }, allocator);
    child.cwd = sdkPath("/");
    child.stderr = std.io.getStdErr();
    child.stdout = std.io.getStdOut();

    _ = try child.spawnAndWait();
}

fn ensureGit(allocator: std.mem.Allocator) void {
    const result = std.ChildProcess.exec(.{
        .allocator = allocator,
        .argv = &.{ "git", "--version" },
    }) catch { // e.g. FileNotFound
        std.log.err("mach: error: 'git --version' failed. Is git not installed?", .{});
        std.process.exit(1);
    };
    defer {
        allocator.free(result.stderr);
        allocator.free(result.stdout);
    }
    if (result.term.Exited != 0) {
        std.log.err("mach: error: 'git --version' failed. Is git not installed?", .{});
        std.process.exit(1);
    }
}
