
const Vec2 = @import("./extern.zig").Vec2;

pub const Entity = struct {
    pos: Vec2,
    vel: Vec2,
    acc: Vec2,
};
