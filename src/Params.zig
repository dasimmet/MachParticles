
pub const Params = packed struct {
    delta_T:          f32 = 0.04,
    bounceDistance:   f32 = 0.03,
    bounceScale:      f32 = 0.75,
    mouse_gravity:    f32 = 0.0,
    speed_limit:      f32 = 0.2,
    particle_gravity: f32 = 0.0,
    unused_2:         f32 = 0.0,
    unused_3:         f32 = 0.0,
    wrap_edges:       f32 = 1.0,
    mouse_x:          f32 = 0.0,
    mouse_y:          f32 = 0.0,

    pub fn toArray(self: *const Params, comptime T: type) []const T {
        return @ptrCast(*const [@bitSizeOf(Params)/@bitSizeOf(T)]T, self);
    }
};