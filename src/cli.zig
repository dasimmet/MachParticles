
const std = @import("std");
const clap = @import("clap");
const builtin = @import("builtin");

const params = clap.parseParamsComptime(
    \\-h, --help             Display this help and exit.
    \\-n, --number <u32>   An option parameter, which takes a value.
    \\-s, --string <str>...  An option parameter which can be specified multiple times.
    \\<str>...
    \\
);

pub fn parse(_: std.mem.Allocator) !clap.Result(clap.Help, &params, clap.parsers.default) {

    // Initalize our diagnostics, which can be used for reporting useful errors.
    // This is optional. You can also pass `.{}` to `clap.parse` if you don't
    // care about the extra information `Diagnostics` provides.
    var diag = clap.Diagnostic{};
    var res = clap.parse(clap.Help, &params, clap.parsers.default, .{
        .diagnostic = &diag,
    }) catch |err| {
        // Report useful error and exit

        switch (builtin.os.tag) {
             .freestanding => {},
             else => {
                diag.report(std.io.getStdErr().writer(), err) catch {};
             },
        }
        return err;
    };
    return res;
}