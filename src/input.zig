
print_frames: bool = false,
monitor_index: usize = 0,
pub const Input = @This();

const std = @import("std");
const mach = @import("mach");
const Params = @import("Params.zig").Params;
const glfw = @import("glfw");

var Modifiers = struct {
    SHIFT: bool = false,
    CTRL:  bool = false,
}{};

var key_scale: f32 = 0.005;
var tmp_mouse_gravity: f32 = 0;

pub fn handle(input: *Input, core: *mach.Core, allocator: *std.mem.Allocator, params: *Params) void {
    var evIt = core.pollEvents();
    while (evIt.next()) |event| {
        switch (event) {
            .key_release => |ev| {
                switch (ev.key) {
                    .left_shift, .right_shift => Modifiers.SHIFT = false,
                    .left_control, .right_control =>  Modifiers.CTRL = false,
                    else => {}
                }
            },
            .key_press, .key_repeat => |ev| {
                std.log.info("Key: {any}", .{ev.key});
                switch (ev.key) {
                    .q, .escape, .space => core.deinit(),
                    .left_shift, .right_shift => Modifiers.SHIFT = true,
                    .left_control, .right_control =>  Modifiers.CTRL = true,
                    .kp_add => {
                        key_scale = key_scale * 2;
                        std.log.info("Key Scale: {any}", .{key_scale});
                    },
                    .kp_subtract => {
                        key_scale = key_scale / 2;
                        std.log.info("Key Scale: {any}", .{key_scale});
                    },
                    .one, .kp_1 => {
                        if (Modifiers.SHIFT){
                            params.bounceDistance -= key_scale;
                        } else {
                            params.bounceDistance += key_scale;
                        }
                    },
                    .two, .kp_2 => {
                        if (Modifiers.SHIFT){
                            params.bounceScale -= key_scale;
                        } else {
                            params.bounceScale += key_scale;
                        }
                    },
                    .three, .kp_3 => {
                        if (Modifiers.SHIFT){
                            params.mouse_gravity -= key_scale;
                        } else {
                            params.mouse_gravity += key_scale;
                        }
                    },
                    .four, .kp_4 => {
                        if (Modifiers.SHIFT){
                            params.speed_limit -= key_scale;
                        } else {
                            params.speed_limit += key_scale;
                        }
                    },
                    .five, .kp_5 => {
                        if (Modifiers.SHIFT){
                            params.particle_gravity -= key_scale;
                        } else {
                            params.particle_gravity += key_scale;
                        }
                    },
                    .six, .kp_6 => {
                        if (Modifiers.SHIFT){
                            params.unused_2 -= key_scale;
                        } else {
                            params.unused_2 += key_scale;
                        }
                    },
                    .seven, .kp_7 => {
                        if (Modifiers.SHIFT){
                            params.unused_3 -= key_scale;
                        } else {
                            params.unused_3 += key_scale;
                        }
                    },
                    .tab => {
                        params.wrap_edges = @mod((params.wrap_edges + 1.0),3.0);
                    },
                    .p => {
                        input.print_frames = !input.print_frames;
                    },
                    .f1 => {
                        const monitorList = glfw.Monitor.getAll(allocator.*) catch @panic("Cannot get Monitor list");
                        defer allocator.*.free(monitorList);

                        const mode = core.displayMode();
                        if (mode != mach.Core.DisplayMode.fullscreen){
                            core.setDisplayMode(mach.Core.DisplayMode.fullscreen, input.monitor_index);
                        } else {
                            if (input.monitor_index >= monitorList.len){
                                input.monitor_index = 0;
                                core.setDisplayMode(mach.Core.DisplayMode.windowed, null);
                            } else {
                                input.monitor_index += 1;
                                core.setDisplayMode(mach.Core.DisplayMode.fullscreen, input.monitor_index);
                            }
                        }
                    },
                    .up => {
                        params.mouse_gravity += key_scale;
                    },
                    .down => {
                        params.mouse_gravity -= key_scale;
                    },
                    else => {}
                }
                std.log.info("{any}", .{params});
            },
            .mouse_scroll => |ev| {
                if (ev.yoffset > 0) {
                    params.mouse_gravity += key_scale;
                } else {
                    params.mouse_gravity -= key_scale;
                }
                std.log.info("{any}", .{params});
            },
            .mouse_motion => |ev| {
                const s = core.size();
                params.mouse_x = 2*@floatCast(f32, ev.pos.x)/@intToFloat(f32, s.width)  - 1;
                params.mouse_y = - 2*@floatCast(f32, ev.pos.y)/@intToFloat(f32, s.height) + 1;
            },
            .mouse_press => |ev| switch (ev.button) {
                .left => {
                    tmp_mouse_gravity = params.mouse_gravity;
                    params.mouse_gravity = -1*params.mouse_gravity;
                },
                .right => {
                    tmp_mouse_gravity = params.mouse_gravity;
                    params.mouse_gravity = 0;
                },
                else => {},
            },
            .mouse_release => |ev| switch (ev.button) {
                .left => {
                    params.mouse_gravity = tmp_mouse_gravity;
                },
                .right => {
                    params.mouse_gravity = tmp_mouse_gravity;
                },
                else => {},
            },
            else => {},
        }
    }
}