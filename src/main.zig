/// A port of Austin Eng's "computeBoids" webgpu sample.
/// https://github.com/austinEng/webgpu-samples/blob/main/src/sample/computeBoids/main.ts
const std = @import("std");
const mach = @import("mach");
const gpu = @import("gpu");
const Entity = @import("Entity.zig").Entity;
const Params = @import("Params.zig").Params;
const cli = @import("cli.zig");
const Input = @import("input.zig").Input;
pub const App = @This();
const build_options = @import("build_options");
const DEFAULT_NUM_PARTICLE: u32 = build_options.default_num_particle;
var gpa = std.heap.GeneralPurposeAllocator(.{}){};
var allocator = gpa.allocator();

core: mach.Core,
compute_pipeline: *gpu.ComputePipeline,
render_pipeline: *gpu.RenderPipeline,
sprite_vertex_buffer: *gpu.Buffer,
particle_buffers: [2]*gpu.Buffer,
particle_bind_groups: [2]*gpu.BindGroup,
param_buffer: *gpu.Buffer,
frame_counter: usize,
entities: []Entity,
title_buffer: [256]u8 = std.mem.zeroes([256]u8),
executable: []u8,
input: Input = Input{},
timer: mach.Timer,

var param_struct = Params{};

pub const log_level = std.log.Level.info;

pub fn init(
    app: *App,
) !void {
    try app.core.init(allocator, .{});

    const clapArgs = try cli.parse(allocator);
    defer clapArgs.deinit();

    // std.log.info("MachArgs: {any}", .{clapArgs.args});

    if (clapArgs.args.help) {
        std.log.info("--help\n", .{});
        app.deinit();
        return;
    }

    // const args: []const u8 = &[0]u8{};
    const args = try std.process.argsAlloc(allocator);
    errdefer allocator.free(args);

    const executable = try allocator.alloc(u8, args[0].len);
    errdefer allocator.free(executable);
    if (args.len > 0) {
        std.mem.copy(u8, executable, args[0]);
    } else {
        std.mem.copy(u8, executable, "MachParticles");
    }

    std.log.info("Args: {any}, Entity Size: {d}", .{ clapArgs.args, @sizeOf(Entity) });

    const num_particle = if (clapArgs.args.number != null)
        clapArgs.args.number.?
    else
        DEFAULT_NUM_PARTICLE;

    std.log.info("Entity Size: {d}, Entity Count: {d}", .{@sizeOf(Entity), num_particle});
    app.core.setSize(.{
        .width = 1280,
        .height = 720,
    });

    app.title_buffer[0] = 0;
    app.core.setTitle(app.title_buffer[0..0 :0]);

    const sprite_shader_module = app.core.device().createShaderModuleWGSL(
        "sprite.wgsl",
        @embedFile("sprite.wgsl"),
    );

    const update_sprite_shader_module = app.core.device().createShaderModuleWGSL(
        "updateSprites.wgsl",
        @embedFile("updateSprites.wgsl"),
    );

    const instanced_particles_attributes = [_]gpu.VertexAttribute{
        .{
            // instance position
            .shader_location = 0,
            .offset = 0,
            .format = .float32x2,
        },
        .{
            // instance velocity
            .shader_location = 1,
            .offset = 2 * 4,
            .format = .float32x2,
        },
    };

    const vertex_buffer_attributes = [_]gpu.VertexAttribute{
        .{
            // vertex positions
            .shader_location = 2,
            .offset = 0,
            .format = .float32x2,
        },
    };

    const render_pipeline = app.core.device().createRenderPipeline(&gpu.RenderPipeline.Descriptor{
        .vertex = gpu.VertexState.init(.{
            .module = sprite_shader_module,
            .entry_point = "vert_main",
            .buffers = &.{
                gpu.VertexBufferLayout.init(.{
                    // instanced particles buffer
                    .array_stride = @sizeOf(Entity),
                    .step_mode = .instance,
                    .attributes = &instanced_particles_attributes,
                }),
                gpu.VertexBufferLayout.init(.{
                    // vertex buffer
                    .array_stride = 2 * 4,
                    .step_mode = .vertex,
                    .attributes = &vertex_buffer_attributes,
                }),
            },
        }),
        .fragment = &gpu.FragmentState.init(.{
            .module = sprite_shader_module,
            .entry_point = "frag_main",
            .targets = &[_]gpu.ColorTargetState{
                .{
                    .format = app.core.descriptor().format,
                },
            },
        }),
    });

    const compute_pipeline = app.core.device().createComputePipeline(&gpu.ComputePipeline.Descriptor{ .compute = gpu.ProgrammableStageDescriptor{
        .module = update_sprite_shader_module,
        .entry_point = "main",
    } });

    const vert_buffer_data = [_]f32{
        -0.01, -0.02, 0.01,
        -0.02, 0.0,   0.02,
    };

    const sprite_vertex_buffer = app.core.device().createBuffer(&gpu.Buffer.Descriptor{
        .label = "sprite_vertex_buffer",
        .usage = .{ .vertex = true, .uniform = true, .copy_dst = true },
        .mapped_at_creation = true,
        .size = vert_buffer_data.len * @sizeOf(f32),
    });
    var vertex_mapped = sprite_vertex_buffer.getMappedRange(f32, 0, vert_buffer_data.len);
    std.mem.copy(f32, vertex_mapped.?, vert_buffer_data[0..]);
    sprite_vertex_buffer.unmap();

    const param_buffer = app.core.device().createBuffer(&gpu.Buffer.Descriptor{
        .label = "param_buffer",
        .usage = .{ .uniform = true, .copy_dst = true },
        .size = @bitSizeOf(Params),
    });

    app.core.device().getQueue().writeBuffer(param_buffer, 0, param_struct.toArray(f32)[0..]);

    const entities = try allocator.alloc(Entity, num_particle);
    errdefer allocator.free(entities);

    const entity_buffer_size = @sizeOf(Entity) * (@intCast(usize, entities.len));

    var rng = std.rand.DefaultPrng.init(0);
    const random = rng.random();

    for (entities) |*ent| {
        ent.pos.x = 2 * (random.float(f32) - 0.5);
        ent.pos.y = 2 * (random.float(f32) - 0.5);
        ent.vel.x = 2 * (random.float(f32) - 0.5) * 0.1;
        ent.vel.y = 2 * (random.float(f32) - 0.5) * 0.1;
    }

    var particle_buffers: [2]*gpu.Buffer = undefined;
    var particle_bind_groups: [2]*gpu.BindGroup = undefined;

    for (&particle_buffers) |*buf| {
        buf.* = app.core.device().createBuffer(&gpu.Buffer.Descriptor{
            .label = "particle_buffer",
            .mapped_at_creation = true,
            .usage = .{
                .vertex = true,
                .storage = true,
            },
            .size = entity_buffer_size,
        });
        var mapped_particles = buf.*.getMappedRange(Entity, 0, entities.len).?;

        std.mem.copy(Entity, mapped_particles, entities);

        buf.*.unmap();
    }

    var i: usize = 0;
    while (i < 2) : (i += 1) {
        particle_bind_groups[i] = app.core.device().createBindGroup(&gpu.BindGroup.Descriptor.init(.{
            .layout = compute_pipeline.getBindGroupLayout(0),
            .entries = &.{
                gpu.BindGroup.Entry.buffer(0, param_buffer, 0, @bitSizeOf(Params)),
                gpu.BindGroup.Entry.buffer(1, particle_buffers[i], 0, entity_buffer_size),
                gpu.BindGroup.Entry.buffer(2, particle_buffers[(i + 1) % 2], 0, entity_buffer_size),
            },
        }));
    }

    app.* = .{
        .core = app.core,
        .entities = entities,
        .executable = executable,
        .timer = try mach.Timer.start(),
        .compute_pipeline = compute_pipeline,
        .render_pipeline = render_pipeline,
        .sprite_vertex_buffer = sprite_vertex_buffer,
        .particle_buffers = particle_buffers,
        .particle_bind_groups = particle_bind_groups,
        .param_buffer = param_buffer,
        .frame_counter = 0,
    };
}

pub fn deinit(app: *App) void {
    app.core.deinit();
    _ = gpa.deinit();
}

fn write_param_buffer(app: *App) void {
    app.core.device().getQueue().writeBuffer(app.param_buffer, 0, param_struct.toArray(f32)[0..]);
}

pub fn update(app: *App) !bool {
    const back_buffer_view = app.core.swapChain().getCurrentTextureView();

    const color_attachment = gpu.RenderPassColorAttachment{
        .view = back_buffer_view,
        .clear_value = std.mem.zeroes(gpu.Color),
        .load_op = .clear,
        .store_op = .store,
    };

    const render_pass_descriptor = gpu.RenderPassDescriptor.init(.{
        .color_attachments = &.{
            color_attachment,
        },
    });
    app.input.handle(&app.core, &allocator, &param_struct);

    param_struct.delta_T = app.timer.lap();
    app.write_param_buffer();

    const command_encoder = app.core.device().createCommandEncoder(null);
    {
        const pass_encoder = command_encoder.beginComputePass(null);
        pass_encoder.setPipeline(app.compute_pipeline);
        pass_encoder.setBindGroup(0, app.particle_bind_groups[app.frame_counter % 2], null);
        pass_encoder.dispatchWorkgroups(@floatToInt(u32, @ceil(@intToFloat(f32, app.entities.len) / 64)), 1, 1);
        pass_encoder.end();
        pass_encoder.release();
    }
    {
        const pass_encoder = command_encoder.beginRenderPass(&render_pass_descriptor);
        pass_encoder.setPipeline(app.render_pipeline);
        pass_encoder.setVertexBuffer(0, app.particle_buffers[(app.frame_counter + 1) % 2], 0, app.entities.len * @sizeOf(Entity));
        pass_encoder.setVertexBuffer(1, app.sprite_vertex_buffer, 0, 6 * @sizeOf(f32));
        pass_encoder.draw(3, @intCast(u32, app.entities.len), 0, 0);
        pass_encoder.end();
        pass_encoder.release();
    }

    app.frame_counter += 1;

    if (app.frame_counter % 30 == 0 and !(app.core.displayMode() == mach.Core.DisplayMode.fullscreen)) {
        app.setTitle();

        if (app.input.print_frames) {
            std.log.info("Frame {}", .{app.frame_counter});
        }
    }

    var command = command_encoder.finish(null);
    command_encoder.release();
    app.core.device().getQueue().submit(&[_]*gpu.CommandBuffer{command});
    command.release();

    app.core.swapChain().present();
    back_buffer_view.release();

    return false;
}

fn setTitle(app: *App) void {
    const title = std.fmt.bufPrint(&app.title_buffer, "{s}, Number of Entities: {d:.0},@{d:.2} fps", .{ app.executable, app.entities.len, 1.0 / app.timer.lap() }) catch @panic("Title string too long");

    app.title_buffer[title.len] = 0; // set sentinel for conversion to C string
}
