struct Particle {
  pos : vec2<f32>,
  vel : vec2<f32>,
  acc : vec2<f32>,
};
struct SimParams {
  deltaT :           f32,
  bounceDistance :   f32,
  bounceScale :      f32,
  mouse_gravity :    f32,
  speed_limit :      f32,
  particle_gravity : f32,
  unused_2 :         f32,
  unused_3 :         f32,
  wrap_edges:        f32,
  mouse_x :          f32,
  mouse_y :          f32,
};
struct Particles {
  particles : array<Particle>,
};
@binding(0) @group(0) var<uniform> params : SimParams;
@binding(1) @group(0) var<storage, read> particlesA : Particles;
@binding(2) @group(0) var<storage, read_write> particlesB : Particles;

// https://github.com/austinEng/Project6-Vulkan-Flocking/blob/master/data/shaders/computeparticles/particle.comp
@compute @workgroup_size(64)
fn main(@builtin(global_invocation_id) GlobalInvocationID : vec3<u32>) {
  var index : u32 = GlobalInvocationID.x;

  var vPos = particlesA.particles[index].pos;
  var vVel = particlesA.particles[index].vel;
  var mPos: vec2<f32>;
  var mVel: vec2<f32>;
  var mDistance: f32;
  var collided: bool;
  var mpos = vec2<f32>(
    params.mouse_x,
    params.mouse_y,
  );
  // apply mouse gravity
  vVel = vVel + params.deltaT * params.mouse_gravity * (mpos-vPos) / (distance(mpos,vPos));

  collided = false;
  for (var i : u32 = 0u; i < arrayLength(&particlesA.particles); i = i + 1u) {
    if (i == index) {
      continue;
    }
    mPos = particlesA.particles[i].pos;
    mVel = particlesA.particles[i].vel;

    mDistance = distance(vPos,mPos);
    // instances attact each other
    // not sure why the if is needed
    if (mDistance < 10){
      vVel -= params.deltaT * params.particle_gravity * normalize(vPos-mPos) / mDistance;
    }

    // elastic collision of close instances
    if (mDistance < params.bounceDistance){
      vVel += params.deltaT * params.bounceScale * length(vVel-mVel) * normalize(vPos-mPos);

      // nudge cases with same position
      if (mDistance == 0){
        vPos.x += bitcast<f32>(index % 10) / 1000;
      }
    }
  }
  // limit velocity
  vVel = normalize(vVel) * clamp(length(vVel), 0.0, params.speed_limit);
  // kinematic update
  vPos = vPos + (vVel * params.deltaT);
  // Wrap around boundary
  if (params.wrap_edges == 1.0){
    if (vPos.x < -1.0) {
      vPos.x = 1.0;
    }
    if (vPos.x > 1.0) {
      vPos.x = -1.0;
    }
    if (vPos.y < -1.0) {
      vPos.y = 1.0;
    }
    if (vPos.y > 1.0) {
      vPos.y = -1.0;
    }
  } else if (params.wrap_edges == 2.0){
    if (vPos.x < -1.0 && vVel.x < 0) {
      vVel.x = -1.0*vVel.x;
    }
    if (vPos.x > 1.0 && vVel.x > 0) {
      vVel.x = -1.0*vVel.x;
    }
    if (vPos.y < -1.0 && vVel.y < 0) {
      vVel.y = -1.0*vVel.y;
    }
    if (vPos.y > 1.0 && vVel.y > 0) {
      vVel.y = -1.0*vVel.y;
    }
  }
  // Write back
  particlesB.particles[index].pos = vPos;
  particlesB.particles[index].vel = vVel;
}
